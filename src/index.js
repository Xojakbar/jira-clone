import React from 'react';
import ReactDOM from 'react-dom';
import App from './jsx/App';
import './styles/styles.scss';

ReactDOM.render(<App />, document.getElementById('root'));
